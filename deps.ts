import { Application, Router, send } from "https://deno.land/x/oak@v6.5.0/mod.ts";
import { viewEngine, engineFactory, adapterFactory } from "https://deno.land/x/view_engine@v1.5.0/mod.ts";
import { Telegram } from "https://deno.land/x/telegram/mod.ts"
import Binance from 'https://esm.sh/binance-api-node?pin=v56'
import { Database, SQLite3Connector, Model, DataTypes, Relationships } from 'https://deno.land/x/denodb@v1.0.40/mod.ts';
import { multiParser } from 'https://deno.land/x/multiparser@v2.1.0/mod.ts'
import { RVI, BollingerBands, StochRSI } from 'https://esm.sh/bfx-hf-indicators'
import * as cheerio from "https://esm.sh/cheerio";
import * as bcrypt from "https://deno.land/x/bcrypt@v0.2.4/mod.ts";