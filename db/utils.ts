import { Database, SQLite3Connector, Model, DataTypes, Relationships } from 'https://deno.land/x/denodb@v1.4.0/mod.ts';

const connector = new SQLite3Connector({
    filepath: './db/database.sqlite',
});
  
export const db = new Database(connector);

export class Account extends Model {
    static table = 'accounts';
    static timestamps = true;
    static fields = {
        id: {
            type: DataTypes.INTEGER,
            primaryKey: true,
        },
        name: DataTypes.STRING,
        emailAddress: DataTypes.STRING,
        password: DataTypes.STRING,
        telegramId: DataTypes.STRING,
        emailNotificationsEnabled: DataTypes.BOOLEAN,
        telegramNotificationsEnabled: DataTypes.BOOLEAN
    }
    id!: string
    name!: string
    emailAddress!:string
    password!:string
    telegramId!:string
    emailNotificationsEnabled!:boolean
    telegramNotificationsEnabled!:boolean
}

export class Signal extends Model {
    static table = 'signals';
    static timestamps = true;
    static fields = {
        id: {
            type: DataTypes.INTEGER,
            primaryKey: true,
        },
        accountId: DataTypes.INTEGER,
        name: DataTypes.STRING,
        action: DataTypes.STRING,
        operator: DataTypes.STRING,
        value: DataTypes.STRING
    }
    static account() {
        return this.hasOne(Account);
    }
    id!: string
    accountId!: string
    name!: string
    action!: string
    operator!: string
    value!: string
    account!: Function
}

export class Spark extends Model {
    static table = 'sparks';
    static timestamps = true;
    static fields = {
        id: {
            type: DataTypes.INTEGER,
            primaryKey: true,
        },
        accountId: DataTypes.INTEGER,
        name: DataTypes.STRING,
        action: DataTypes.STRING,
        operator: DataTypes.STRING,
        value: DataTypes.STRING
    }
    static account() {
        return this.hasOne(Account);
    }
    id!: string
    accountId!: string
    name!: string
    action!: string
    operator!: string
    value!: string
    account!: Function
}

export class Update extends Model {
    static table = 'updates';
    static timestamps = true;
    static fields = {
        id: {
            type: DataTypes.INTEGER,
            primaryKey: true,
        },
        bigVersion: DataTypes.INTEGER,
        smallVersion: DataTypes.INTEGER,
        changes: DataTypes.STRING
    }
    id!: string
    bigVersion!: string
    smallVersion!: string
    changes!: string
}

Relationships.belongsTo(Signal, Account);
Relationships.belongsTo(Spark, Account);
db.link([Account, Signal, Spark, Update])
db.sync()
