import { Router } from "https://deno.land/x/oak@v10.6.0/mod.ts";
import { multiParser } from 'https://deno.land/x/multiparser@v0.105.0/mod.ts'
import * as bcrypt from "https://deno.land/x/bcrypt@v0.4.1/mod.ts";
import { Account, Signal, Spark, Update } from "../db/utils.ts"

export const router = new Router();

router.get("/", async (ctx, next) => {
    const updates = (await Update.orderBy('created_at', 'desc').all() as Update[])
    //console.log(updates)
    ctx.render("index", { data: { name: "John" }, updates: updates });
})

router.get("/login", (ctx, next) => {
    ctx.render("login", { data: { name: "John" } });
})

router.get("/settings", async (ctx, next) => {
    if(typeof ctx.state.accountId === 'undefined'){
        ctx.response.redirect("/login")
    } else {
        const account = await Account.find(ctx.state.accountId)
        //console.log(account)
        if(typeof account === 'undefined'){
            ctx.response.redirect("/login")
        } else {
            if(account.emailNotificationsEnabled == true) {
                account.emailNotificationsEnabled = "checked"
            } 
            if(account.telegramNotificationsEnabled == true) {
                account.telegramNotificationsEnabled = "checked"
            } 
            ctx.render("settings", { data: account });
        }
    }
})

router.get("/signals",async (ctx, next) => {
    if(typeof ctx.state.accountId === 'undefined'){
        ctx.response.redirect("/login")
    } else {
        const signals = await Signal.where('accountId',ctx.state.accountId).get()
        for(const signal of (signals as Signal[])){
            switch (signal.name) {
                case "MayerMultiple":
                    signal.MayerMultiple = true
                    break;
            
                case "MayerBands":
                    signal.MayerBands = true
                    switch (signal.value) {
                        case "Overbought":
                            signal.overbought = true
                            break;
                        case "Bullish extension":
                            signal.bullish_ext = true
                            break;
                        case "Bullish":
                            signal.bullish = true
                            break;
                        case "Bearish":
                            signal.bearish = true
                            break;
                        case "Oversold":
                            signal.oversold = true
                            break;
                    }
                    break;
                case "RVI":
                    signal.RVI = true
                    break;
                case "MVRV":
                    signal.MVRV = true
                    break;
                case "MVRVZ":
                    signal.MVRVZ = true
                    break;
            }
            switch (signal.action) {
                case "Buy":
                    signal.buy = true
                    break;
                case "Sell":
                    signal.sell = true
                    break;
                case "Advise":
                    signal.advise = true
                    break;
            }
            switch (signal.operator) {
                case "Is":
                    signal.is = true
                    break;
                case "Lower":
                    signal.lower = true
                    break;
                case "Equal or Higher":
                    signal.equal_higher = true
                    break;
                case "Equal or Lower":
                    signal.equal_lower = true
                    break;
                case "Higher":
                    signal.higher = true
                    break;
                case "Cross Higher":
                    signal.crossHigher = true
                    break;
                case "Cross Lower":
                    signal.crossLower = true
                    break;
            }
        }
        //console.log(signals)
        ctx.render("signals", { data: { name: "John" }, signals: signals });
    }
})

router.get("/sparks",async (ctx, next) => {
    if(typeof ctx.state.accountId === 'undefined'){
        ctx.response.redirect("/login")
    } else {
        const sparks = await Spark.where('accountId',ctx.state.accountId).get()
        for(const spark of (sparks as Signal[])){
            switch (spark.name) {
                case "BollingerBands":
                    spark.BollingerBands = true
                    switch (spark.value) {
                        case "Top":
                            spark.top = true
                            break;
                        case "Middle":
                            spark.middle = true
                            break;
                        case "Bottom":
                            spark.bottom = true
                            break;
                    }
                    break;
                case "StochRSI":
                    spark.StochRSI = true
                    break;
            }
            switch (spark.action) {
                case "Long":
                    spark.long = true
                    break;
                case "Short":
                    spark.short = true
                    break;
                case "Advise":
                    spark.advise = true
                    break;
            }
            switch (spark.operator) {
                case "Is":
                    spark.is = true
                    break;
                case "Lower":
                    spark.lower = true
                    break;
                case "Equal or Higher":
                    spark.equal_higher = true
                    break;
                case "Equal or Lower":
                    spark.equal_lower = true
                    break;
                case "Higher":
                    spark.higher = true
                    break;
                case "Cross Higher":
                    spark.crossHigher = true
                    break;
                case "Cross Lower":
                    spark.crossLower = true
                    break;
            }
        }
        //console.log(signals)
        ctx.render("sparks", { data: { name: "John" }, sparks: sparks });
    }
})

router.post("/postLogin",async (ctx, next) => {
    const form = await multiParser(ctx.request.serverRequest)
    let account = await Account.where('emailAddress', form!.fields.emailAddress).get()
    console.log(form?.fields, account.length, account)
    if(account.length === 0){
        //no account found, creating one
        account = await Account.create([
            {
                emailAddress: form!.fields.emailAddress,
                password: await bcrypt.hash(form!.fields.password)
            }
        ])
        account = (await Account.where('emailAddress', form!.fields.emailAddress).get() as Account[])
        ctx.state.accountId = account[0].id
        console.log("new account created")
        ctx.response.redirect("/settings")
    }else{
        // account created, verify password
        if(await bcrypt.compare(form!.fields.password, (account as Account[])[0].password)){
            // good password, login
            ctx.state.accountId = (account as Account[])[0].id
            console.log("good password!")
            ctx.response.redirect("/settings")
        }else {
            // bad password
            ctx.state.accountId = null
            console.log("bad password!")
            ctx.response.redirect("/login")
        }
    }

})

router.post("/postSettings",async (ctx, next) => {
    if(typeof ctx.state.accountId === 'undefined'){
        ctx.response.redirect("/login")
    } else {
        const form = await multiParser(ctx.request.serverRequest)
    /*   form.fields:{
            emailNotificationsEnabled: "",
            emailAddress: "test@tets.ca",
            telegramId: "",
            password: "kjhhoisd"
        } */
        console.log(form!.fields)
        let emailNotificationsEnabled = false
        if(typeof form?.fields.emailNotificationsEnabled !== "undefined") {
            form.fields.emailNotificationsEnabled = "checked"
            emailNotificationsEnabled = true
        } 
        let telegramNotificationsEnabled = false
        if(typeof form?.fields.telegramNotificationsEnabled !== "undefined" ) {
            form.fields.telegramNotificationsEnabled = "checked"
            telegramNotificationsEnabled = true
        } 
        const account = await Account.find(ctx.state.accountId)
        account.emailAddress = form!.fields.emailAddress
        account.telegramId = form!.fields.telegramId
        account.emailNotificationsEnabled = emailNotificationsEnabled
        account.telegramNotificationsEnabled = telegramNotificationsEnabled
        await account.update()
        
        ctx.response.redirect("/settings")
    }
})

router.post("/postSignals",async (ctx, next) => {
    if(typeof ctx.state.accountId === 'undefined'){
        ctx.response.redirect("/login")
    } else {
        const form = await multiParser(ctx.request.serverRequest)
        //console.log(form?.fields)
        if(form!.fields.submit === "add"){
            // signals to add
            if(form?.fields.MayerMultipleEnable === "on") {
                // activate MayerMultiple
                await Signal.create([
                    {
                        accountId: ctx.state.accountId,
                        name: "MayerMultiple",
                        action: form?.fields.MayerMultipleAction,
                        operator: form?.fields.MayerMultipleOperator,
                        value: form?.fields.MayerMultipleValue,
                    }
                ])
            } 
            if(form?.fields.MayerBandsEnable === "on") {
                // activate MayerMultiple
                await Signal.create([
                    {
                        accountId: ctx.state.accountId,
                        name: "MayerBands",
                        action: form?.fields.MayerBandsAction,
                        operator: form?.fields.MayerBandsOperator,
                        value: form?.fields.MayerBandsValue,
                    }
                ])
            } 
            if(form?.fields.RVIEnable === "on") {
                // activate MayerMultiple
                await Signal.create([
                    {
                        accountId: ctx.state.accountId,
                        name: "RVI",
                        action: form?.fields.RVIAction,
                        operator: form?.fields.RVIOperator,
                        value: form?.fields.RVIValue,
                    }
                ])
            } 
            if(form?.fields.MVRVEnable === "on") {
                // activate MayerMultiple
                await Signal.create([
                    {
                        accountId: ctx.state.accountId,
                        name: "MVRV",
                        action: form?.fields.MVRVAction,
                        operator: form?.fields.MVRVOperator,
                        value: form?.fields.MVRVValue,
                    }
                ])
            } 
            if(form?.fields.MVRVZEnable === "on") {
                // activate MayerMultiple
                await Signal.create([
                    {
                        accountId: ctx.state.accountId,
                        name: "MVRVZ",
                        action: form?.fields.MVRVZAction,
                        operator: form?.fields.MVRVZOperator,
                        value: form?.fields.MVRVZValue,
                    }
                ])
            } 
        } else if (form!.fields.submit === "update"){
            //console.log(form!.fields)
            const fields = form!.fields
            console.log(fields)
            //create a list of acive signals
            for (const [key, value] of Object.entries(fields)) {
                let htmlId = 0
                if(key.includes("SignalId")){
                    htmlId = Number(key.match(/\d+/)![0])
                    const dbSignal = await Signal.find(value)
                    if(typeof fields["Enable-"+htmlId] == "undefined"){
                        await dbSignal.delete()
                    }else{
                        dbSignal.action = fields["Action-"+htmlId]
                        dbSignal.operator = fields["Operator-"+htmlId]
                        dbSignal.value = fields["Value-"+htmlId]
                        await dbSignal.update()
                    }
                }
            }
        }
        ctx.response.redirect("/signals")
    }
})

router.post("/postSparks",async (ctx, next) => {
    if(typeof ctx.state.accountId === 'undefined'){
        ctx.response.redirect("/login")
    } else {
        const form = await multiParser(ctx.request.serverRequest)
        //console.log(form?.fields)
        if(form!.fields.submit === "add"){
            // signals to add
            if(form?.fields.StochRSIEnable === "on") {
                // activate StochRSI
                await Spark.create([
                    {
                        accountId: ctx.state.accountId,
                        name: "StochRSI",
                        action: form?.fields.StochRSIAction,
                        operator: form?.fields.StochRSIOperator,
                        value: form?.fields.StochRSIValue,
                    }
                ])
            } 
            if(form?.fields.BollingerBandsEnable === "on") {
                // activate StochRSI
                await Spark.create([
                    {
                        accountId: ctx.state.accountId,
                        name: "BollingerBands",
                        action: form?.fields.BollingerBandsAction,
                        operator: form?.fields.BollingerBandsOperator,
                        value: form?.fields.BollingerBandsValue,
                    }
                ])
            } 
        } else if (form!.fields.submit === "update"){
            //console.log(form!.fields)
            const fields = form!.fields
            console.log(fields)
            //create a list of acive signals
            for (const [key, value] of Object.entries(fields)) {
                let htmlId = 0
                if(key.includes("SignalId")){
                    htmlId = Number(key.match(/\d+/)![0])
                    const dbSpark = await Spark.find(value)
                    if(typeof fields["Enable-"+htmlId] == "undefined"){
                        await dbSpark.delete()
                    }else{
                        dbSpark.action = fields["Action-"+htmlId]
                        dbSpark.operator = fields["Operator-"+htmlId]
                        dbSpark.value = fields["Value-"+htmlId]
                        await dbSpark.update()
                    }
                }
            }
        }
        ctx.response.redirect("/sparks")
    }
})