import { send } from "https://deno.land/x/oak@v10.6.0/mod.ts";

export const staticFileMiddleware = async (ctx: any, next: Function) => {
  let pathName: string = ctx.request.url.pathname;
  const path = `${Deno.cwd()}/public${pathName}`;
  //console.log(pathName, path)

  if (await fileExists(path)) {
    await send(ctx, pathName, {
      root: `${Deno.cwd()}/public`,
    });
    //console.log('File Exists')
  } else {
    await next();
  }
};

async function fileExists(path: string) {
  try {
    const stats = await Deno.lstat(path);
    return stats && stats.isFile;
  } catch (e) {
    if (e && e instanceof Deno.errors.NotFound) {
      return false;
    } else {
      throw e;
    }
  }
}