import { Account, Signal, db } from "../db/utils.ts"
import Binance from 'https://esm.sh/binance-api-node?pin=v56'
import { RVI } from 'https://esm.sh/bfx-hf-indicators'
import { cheerio } from "https://deno.land/x/cheerio@1.0.4/mod.ts";
//import { serialize, deserialize } from "https://deno.land/x/serialize_javascript@v1.0.1/mod.ts";
//import { serialize, deserialize } from "https://deno.land/x/serialize_simple@v1.1.0/mod.ts";

const client = Binance()
interface candle {
    openTime: number,
    open: string,
    high: string,
    low: string,
    close: string,
    volume: string,
    closeTime: number,
    quoteVolume: string,
    trades: number,
    baseAssetVolume: string,
    quoteAssetVolume: string
}
interface result {
    name: string, 
    value: number
}
import { Telegram } from "https://deno.land/x/telegram/mod.ts"

await trySignals()
Deno.exit()

async function trySignals(){
    const signals = (await Signal.all() as Signal[])
    console.log((new Date).toUTCString()+"Trying "+signals.length+" signals")
    const MayerMultiple = await calculateMayerMultiple()
    const MayerBands = convertMultipleToBands(MayerMultiple)
    const RVI = await calculateRelativeVolatillityIndex()
    //const MVRV = await calculateMVRV()
    const MVRVZ = await calculateMVRVZscore()
    console.log(MayerMultiple, MayerBands, RVI, MVRVZ)
    for (const signal of signals){
        // dispatch per name, then worker per notifs
        signal.result = "Unmatched"
        if(signal.action === "Advise"){
            signal.result = "Advise"
        }
        let calculatedValues = eval(signal.name) as result[]
        if(signal.name === "MayerBands"){
            if(compareBandsWithOperator(calculatedValues, signal.operator, signal.value)){
                // signal is true, signalling
                signal.result = "Matched"
            }
        }else{
            if(compareWithOperator(calculatedValues, signal.operator, Number(signal.value))){
                // signal is true, signalling
                signal.result = "Matched"
            }
        }
        
        console.log(signal.name, calculatedValues, signal.value)
        await notify(signal, calculatedValues)
    }
    console.log("Tried all signals")
    db.close()
    return 0
}

async function notify(signal:Signal, calculatedValues: result[]) {
    const account = (await Signal.where('id', signal.id).account() as Account)
    if(signal.result === "Matched"){
        console.log("Signal "+ signal.id + " is true, signalling. EmailEnabled: "+account.emailNotificationsEnabled+" TelegramEnabled: "+account.telegramNotificationsEnabled)
    }
    if(account.emailNotificationsEnabled){
        // child process send email notification
    }
    if(account.telegramNotificationsEnabled){
        // child process send telegram notification
        await sendTelegramNotification(account.telegramId, signal, calculatedValues)
    }
}

async function sendTelegramNotification(telegramId: string, signal: Signal, values: result[]){
    // create bot instance
    let adviseString = ""
    switch (signal.action) {
        case "Advise":
            adviseString = "I'm advising, "
            break;
        case "Buy":
            adviseString = "You should Buy, "
            break;
        case "Sell":
            adviseString = "You should Sell, "
            break;
    }
    let beautifiedName = ""
    switch(signal.name){
        case "MayerMultiple":
            beautifiedName = "Mayer Multiple"
        break;
        case "MayerBands":
            beautifiedName = "Mayer Bands"
        break;
        case "RVI":
            beautifiedName = "Relative Volatility Index"
        break;
        case "MVRV":
            beautifiedName = "MVRV Ratio"
        break;
        case "MVRVZ":
            beautifiedName = "MVRV Z-Score"
        break;
        default:
            beautifiedName = signal.name
        break;
    }
    let string = ""
    if(signal.result === "Matched"){
        switch (signal.operator) {
            case "Is":
                string = String(adviseString + beautifiedName+" is "+ signal.value + " : " + values[1].name)
                break;
            case "Cross Lower":
                string = String(adviseString + beautifiedName+" has crossed lower than " + signal.value + " : " + values[1].name)
                break;
            case "Cross Higher":
                string = String(adviseString + beautifiedName+" has crossed higher than " + signal.value + " : " + values[1].name)
                break;
            default:
                string = String(adviseString + beautifiedName+" is "+ signal.operator + " than " + signal.value + " : " + values[1].name)
                break;
        }
    }else if(signal.result === "Advise"){
        switch (signal.operator) {
            case "Is":
                string = String(adviseString + beautifiedName+" is not yet "+ signal.value + " : " + values[1].name)
                break;
            case "Cross Lower":
                string = String(adviseString + beautifiedName+" has not yet crossed lower than " + signal.value + " : " + values[1].name)
                break;
            case "Cross Higher":
                string = String(adviseString + beautifiedName+" has not yet crossed higher than " + signal.value + " : " + values[1].name)
                break;
            default:
                string = String(adviseString + beautifiedName+" is not yet "+ signal.operator + " than " + signal.value + " : " + values[1].name)
                break;
        }
    }
    if(signal.result !== "Unmatched"){
        console.log("sending Telegram to: " + telegramId)
        const bot = new Telegram("2033019889:AAFWKUYMcLBp6cjGJQzto0qH4QewW1TQgjo")
            await bot.sendMessage({
            chat_id: telegramId,
            text: string,
            });
            console.log("Telegram Notification Sent")
        }
    return 0
}

async function calculateMayerMultiple() {
    // get historical klines, 1D, 200 days ago utc
    const klines = (await client.candles({symbol: 'BTCUSDT', interval: '1d', startTime: (new Date(new Date())).setDate(new Date().getUTCDate()-200)}) as candle[])
    let tempPrice = 0
    let prevTempPrice = 0
    for (const kline of klines){
        prevTempPrice = tempPrice
        tempPrice = Number(tempPrice + Number(kline.close))
    }
    const SMA = Number(tempPrice/200)
    const prevSMA = Number(prevTempPrice/200)
    const mayer = Number(Number(klines[klines.length-1].close)/SMA)
    const prevMayer = Number(Number(klines[klines.length-2].close)/prevSMA)
    return [
        {
            name: prevMayer.toFixed(2), 
            value: Number(prevMayer.toFixed(2))
        }, {
            name: mayer.toFixed(2),
            value: Number(mayer.toFixed(2))
        }
    ] as result[]
}

async function calculateRelativeVolatillityIndex() {
    // get historical klines, 2w, 20w ago utc
    // >90 <30, period 10 D  fro 2week candles
    const klines = (await client.candles({symbol: 'BTCUSDT', interval: '1w', startTime: (new Date(new Date())).setDate(new Date().getUTCDate()-147)}) as candle[])
    // calculate standard deviation (period?)
    // Calculate the simple average (mean) of the closing price (i.e., add the last 10 closing prices and divide the total by 10).
    const rvi = new RVI([10])
    let prevRVI = 0
    for (const [i, kline] of klines.entries()){
        rvi.add(Number(kline.close))
        //console.log(i,klines.length)
        if(i == (klines.length - 2)){
            // previous one
            prevRVI = await rvi.v()
        }
    }
    const rviValue = await rvi.v()
    return [
        {
            name: prevRVI.toFixed(2), 
            value: Number(prevRVI.toFixed(2))
        }, {
            name: rviValue.toFixed(2),
            value: Number(rviValue.toFixed(2))
        }
    ] as result[]
}

/* async function calculateMVRV(){
    const url = 'https://charts.woobull.com/bitcoin-mvrv-ratio/' //'https://stats.buybitcoinworldwide.com/realized-price/'
    let mvrv = [0]
    try {
        const res = await fetch(url);
        const html = await res.text();
        const $ = cheerio.load(html)  
        const pageText = $('body > script').html()
        //console.log(pageText)
        const match = (serialize(String(serialize(pageText!)!.match(/mvrv = (\{.*?\{)/)![0]!))).replace('mvrv = ', '').replace(/\\n/g, '').replace(/\\/g, '').replace(",   marker: {","")
        const parsed = eval('(' + deserialize(match) + '})')
        const mvrv = parsed.y
        //console.log("Match", parsed)
        return [
            {
                name: Number(mvrv[ mvrv.length - 2 ]).toFixed(2), 
                value: Number(Number(mvrv[ mvrv.length - 2 ]).toFixed(2))
            }, {
                name: Number(mvrv[ mvrv.length - 1 ]).toFixed(2),
                value: Number(Number(mvrv[ mvrv.length - 1 ]).toFixed(2))
            }
        ] as result[]
    } catch(error) {
        console.log(error);
    }
} */

async function calculateMVRVZscore(){
    const url = 'https://bitcoinition.com/charts/mvrv-z-score/' //'https://stats.buybitcoinworldwide.com/realized-price/'
    let mvrvz = [0]
    try {
        const res = await fetch(url);
        const html = await res.text();
        const $ = cheerio.load(html)  
        const pageText = $('#DivMVRVZscore').html()
        const match = (pageText!.match(/var y2axis = (\[.*?\])/)![0]).replace("var y2axis = ","")
        const mvrvz = JSON.parse(match)
        //console.log("Match", parsed)
        return [
            {
                name: Number(mvrvz[ mvrvz.length - 2 ]).toFixed(2), 
                value: Number(Number(mvrvz[ mvrvz.length - 2 ]).toFixed(2))
            }, {
                name: Number(mvrvz[ mvrvz.length - 1 ]).toFixed(2),
                value: Number(Number(mvrvz[ mvrvz.length - 1 ]).toFixed(2))
            }
        ] as result[]
    } catch(error) {
        console.log(error);
    }
}

function convertMultipleToBands(origValues: result[]){
    let values = JSON.parse(JSON.stringify(origValues));
    // previous result
    switch (true) {
        case (values[0].value > 2.5):
            values[0].name = "Overbought"
            values[0].value = 5
            break;
        case (values[0].value > 1.7 && values[0].value < 2.5):
            values[0].name = "Bullish extension"
            values[0].value = 4
            break;
        case (values[0].value > 1.1 && values[0].value < 1.7):
            values[0].name = "Bullish"
            values[0].value = 3
            break;
        case (values[0].value > 0.55 && values[0].value < 1.1):
            values[0].name = "Bearish"
            values[0].value = 2
            break;
        case (values[0].value < 0.55):
            values[0].name = "Oversold"
            values[0].value = 1
            break;
    }
    //result
    switch (true) {
        case (values[1].value > 2.5):
            values[1].name = "Overbought"
            values[1].value = 5
            break;
        case (values[1].value > 1.7 && values[1].value < 2.5):
            values[1].name = "Bullish extension"
            values[1].value = 4
            break;
        case (values[1].value > 1.1 && values[1].value < 1.7):
            values[1].name = "Bullish"
            values[1].value = 3
            break;
        case (values[1].value > 0.55 && values[1].value < 1.1):
            values[1].name = "Bearish"
            values[1].value = 2
            break;
        case (values[1].value < 0.55):
            values[1].name = "Oversold"
            values[1].value = 1
            break;
    }
    //console.log("convertMultipleToBands",values)
    return values
}

function compareWithOperator(values:result[], operator: string, signalValue:number) {
    let result = false
    switch (operator) {
        case "Is":
            if(values[1].value == signalValue){
                result = true;
            }
            break;
        case "Equal or Higher":
            if(values[1].value >= signalValue){
                result = true;
            }
            break;
        case "Lower":
            if(values[1].value < signalValue){
                result = true;
            }
            break;
        case "Equal or Lower":
            if(values[1].value <= signalValue){
                result = true;
            }
            break;
        case "Higher":
            if(values[1].value > signalValue){
                result = true;
            }
            break;
        case "Cross Lower":
            if((values[0].value >= signalValue) && (values[1].value < signalValue)){
                result = true;
            }
            break;
        case "Cross Higher":
            if((values[0].value <= signalValue) && (values[1].value > signalValue)){
                result = true;
            }
            break;
    }
    return result
}

function compareBandsWithOperator(values:result[], operator: string, signalValue:string) {
    let signalBandsValue = 0
    switch (signalValue) {
        case "Overbought":
            signalBandsValue = 5
            break;
        case "Bullish extension":
                signalBandsValue = 4
                break;
        case "Bullish":
            signalBandsValue = 3
            break;
        case "Bearish":
            signalBandsValue = 2
            break;
        case "Oversold":
            signalBandsValue = 1
            break;
    }
    //console.log("signalBandsValue", signalBandsValue)
    return compareWithOperator(values, operator, signalBandsValue)
}