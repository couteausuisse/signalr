import { Update, Account } from "../db/utils.ts"
import EventEmitter from "https://deno.land/x/events/mod.ts";
import { Telegram } from "https://deno.land/x/@v0.1.1/mod.ts"


const bigVersion = 0
const smallVersion = 5
const changes = "Initial work on Sparks, intra-day leveraged trading helpers. Removed MVRV signal."

export async function checkUpdate(){
    const latestUpdate = (await Update.orderBy('created_at', 'desc').first() as Update)
    let notif = false
    let updating = false
    if(typeof latestUpdate === "undefined"){
        updating = true
        notif = true
    } else {
        if(Number(bigVersion) > Number(latestUpdate.bigVersion)){
            // new big version, updating
            updating = true
            notif = true
            console.log("bigNumber")
        }
        if(Number(smallVersion) !== Number(latestUpdate.smallVersion)){
            // new small version, updating
            updating = true
            notif = true
            console.log("smallNumber")
        }
    }
    if(updating){
        Update.create({
            bigVersion: bigVersion,
            smallVersion: smallVersion,
            changes: changes
        })
        console.log("Updated")
    }
    if(notif){
        console.log("Notified")
        const accounts = (await Account.all() as Account[])
        for(const account of accounts){
            const bot = new Telegram("2033019889:AAFWKUYMcLBp6cjGJQzto0qH4QewW1TQgjo")
            await bot.sendMessage({
                chat_id: account.telegramId,
                text: String("New Update ("+bigVersion+"."+smallVersion+"): "+changes)
            });
        }
    }
    return 0
}
