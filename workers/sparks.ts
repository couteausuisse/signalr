import { Account, Spark, db } from "../db/utils.ts"
import Binance from 'https://esm.sh/binance-api-node?pin=v56'
import { BollingerBands, StochRSI } from 'https://esm.sh/bfx-hf-indicators'

const client = Binance()
interface candle {
    openTime: number,
    open: string,
    high: string,
    low: string,
    close: string,
    volume: string,
    closeTime: number,
    quoteVolume: string,
    trades: number,
    baseAssetVolume: string,
    quoteAssetVolume: string
}
interface result {
    name: string, 
    value: number
}
import { Telegram } from "https://deno.land/x/telegram/mod.ts"

await trySparks()
Deno.exit()


async function trySparks(){
    // loop by users
    const accounts = (await Account.all() as Spark[])

    for (const account of accounts) {
        const sparks = (await Spark.all() as Spark[])
        console.log((new Date).toUTCString()+"Trying "+sparks.length+" sparks")
        const BollingerBands = await calculateBollingerBands()
        const StochRSI = await calculateStochRSI()
        const price = (await client.prices({symbol: "BTCUSDT"})).BTCUSDT
        console.log(BollingerBands[1], StochRSI[1], price)
        let longs = [] as Spark[]
        let shorts = [] as Spark[]
        let advises = [] as Spark[]
        for (const spark of sparks){
            let calculatedValues = eval(spark.name) as result[]
            switch (spark.name) {
                case "BollingerBands":
                    if(compareBandsWithOperator(calculatedValues, price, spark)){
                        // signal is true, signalling
                        spark.result = "Matched"
                    }
                    break;
            
                case "StochRSI":
                    if(compareWithOperator(calculatedValues, spark.operator, Number(spark.value))){
                        // signal is true, signalling
                        spark.result = "Matched"
                    }
                    break;
            }
            // dispatch per action
            switch (spark.action) {
                case "Long":
                    longs.push(spark)
                    break;
                case "Short":
                    shorts.push(spark)
                    break;
                case "Advise":
                    spark.result = "Matched"
                    advises.push(spark)
                    break;
            }
        }

        for (const array of [advises, longs, shorts]) {
            if(array.length === (array.filter(spark => spark.result == "Matched")).length && array.length > 0){
                // all array conditions are matched
                //console.log(array)
                //console.log(array[0])
                await notify(array[0], price)
            }
        }
    
        console.log("Tried all sparks for account", account.emailAddress )
    }
    db.close()
    return 0
}

async function notify(spark: Spark, price: number) {
    const account = (await Spark.where('id', spark.id).account() as Account)
    if(account.emailNotificationsEnabled){
        // child process send email notification
    }
    if(account.telegramNotificationsEnabled){
        // child process send telegram notification
        await sendTelegramNotification(account.telegramId, spark, price)
    }
} 

async function sendTelegramNotification(telegramId: string, spark: Spark, price:number){
    // create bot instance
    let adviseString = ""
    switch (spark.action) {
        case "Advise":
            adviseString = "I'm advising, "
            break;
        case "Long":
            adviseString = "You should LONG, "
            break;
        case "Short":
            adviseString = "You should SHORT, "
            break;
    }
    let string = ""
    if(spark.result === "Matched"){
        string = String(adviseString + " all conditions are true, at "+ Number(price).toFixed(0)+"$")
    }else if(spark.result === "Advise"){
        string = String(adviseString +" all conditions are ignored, at "+ Number(price).toFixed(0)+"$")
    }
    console.log("sending Telegram to: " + telegramId)
    const bot = new Telegram("2033019889:AAFWKUYMcLBp6cjGJQzto0qH4QewW1TQgjo")
        await bot.sendMessage({
        chat_id: telegramId,
        text: string,
        });
        console.log("Telegram Notification Sent")
    return 0
}

async function calculateBollingerBands() {
    // get historical klines, 2w, 20w ago utc
    // >90 <30, period 10 D  fro 2week candles
    //console.log("getting klines")
    const klines = (await client.candles({symbol: 'BTCUSDT', interval: '1h', startTime: (new Date(new Date())).setDate(new Date().getUTCDate()-20)}) as candle[])
    // calculate standard deviation (period?)
    // Calculate the simple average (mean) of the closing price (i.e., add the last 10 closing prices and divide the total by 10).
    //console.log(klines)
    const bb = new BollingerBands()
    let prevBB = {top:0, middle:0, bottom:0}
    for (const [i, kline] of klines.entries()){
        bb.add(Number(kline.close))
        //console.log(i,klines.length)
        if(i == (klines.length - 2)){
            // previous one
            prevBB = await bb.v() as {top:number, middle:number, bottom:number}
        }
    }
    const bbValue = await bb.v() as {top:number, middle:number, bottom:number}
    //console.log(bbValue)
    return [
        {
            name: "Prev-BB-top", 
            value: Number(prevBB.top.toFixed(2))
        }, {
            name: "BB-top",
            value: Number(bbValue.top.toFixed(2))
        },
        {
            name: "Prev-BB-middle", 
            value: Number(prevBB.middle.toFixed(2))
        }, {
            name: "BB-middle",
            value: Number(bbValue.middle.toFixed(2))
        },
        {
            name: "Prev-BB-bottom", 
            value: Number(prevBB.bottom.toFixed(2))
        }, {
            name: "BB-bottom",
            value: Number(bbValue.bottom.toFixed(2))
        }
    ] as result[]
}

async function calculateStochRSI() {
    // get historical klines, 2w, 20w ago utc
    // >90 <30, period 10 D  fro 2week candles
    //console.log("getting klines")
    const klines = (await client.candles({symbol: 'BTCUSDT', interval: '1h', startTime: (new Date(new Date())).setDate(new Date().getUTCDate()-17)}) as candle[])
    // calculate standard deviation (period?)
    // Calculate the simple average (mean) of the closing price (i.e., add the last 10 closing prices and divide the total by 10).
    //console.log(klines)
    const srsi = new StochRSI([14, 14, 3, 3])
    let prevsrsi = {v:0, signal:0}
    for (const [i, kline] of klines.entries()){
        srsi.add(Number(kline.close))
        //console.log(i,klines.length)
        if(i == (klines.length - 2)){
            // previous one
            prevsrsi = await srsi.v() as {v:number, signal:number}
        }
    }
    const srsiValue = await srsi.v() as {v:number, signal:number}
    //console.log(srsiValue)
    return [
        {
            name: "Prev-StochRSI-v", 
            value: Number(prevsrsi.v.toFixed(2))
        }, {
            name: "StochRSI-v",
            value: Number(srsiValue.v.toFixed(2))
        },
        {
            name: "Prev-StochRSI-Signal", 
            value: Number(prevsrsi.signal.toFixed(2))
        }, {
            name: "AtochRSI-Signal",
            value: Number(srsiValue.signal.toFixed(2))
        }
    ] as result[]
}

function compareWithOperator(values:result[], operator: string, signalValue:number) {
    let result = false
    switch (operator) {
        case "Is":
            if(values[1].value == signalValue){
                result = true;
            }
            break;
        case "Equal or Higher":
            if(values[1].value >= signalValue){
                result = true;
            }
            break;
        case "Lower":
            if(values[1].value < signalValue){
                result = true;
            }
            break;
        case "Equal or Lower":
            if(values[1].value <= signalValue){
                result = true;
            }
            break;
        case "Higher":
            if(values[1].value > signalValue){
                result = true;
            }
            break;
        case "Cross Lower":
            if((values[0].value >= signalValue) && (values[1].value < signalValue)){
                result = true;
            }
            break;
        case "Cross Higher":
            if((values[0].value <= signalValue) && (values[1].value > signalValue)){
                result = true;
            }
            break;
    }
    return result
}

function compareBandsWithOperator(calculatedValues:result[], price: number, spark: Spark) {
    let sparkBandsValue = 0
    let sparkPrevBandsValue = 0
    let result = false
    switch (spark.value) {
        case "Top":
            sparkBandsValue = calculatedValues[1].value
            sparkPrevBandsValue = calculatedValues[0].value
            break;
        case "Middle":
            sparkBandsValue = calculatedValues[3].value
            sparkPrevBandsValue = calculatedValues[2].value
            break;
        case "Bottom":
            sparkBandsValue = calculatedValues[5].value
            sparkPrevBandsValue = calculatedValues[4].value
            break;
    }
    //console.log(sparkBandsValue, spark.operator, price)
    switch (spark.operator) {
        case "Is":
            if(sparkBandsValue == price){
                result = true;
            }
            break;
        case "Equal or Higher":
            if(price >= sparkBandsValue){
                result = true;
            }
            break;
        case "Lower":
            if(price < sparkBandsValue){
                result = true;
            }
            break;
        case "Equal or Lower":
            if(price <= sparkBandsValue){
                result = true;
            }
            break;
        case "Higher":
            if(price > sparkBandsValue){
                result = true;
            }
            break;
        case "Cross Lower":
            if((price >= sparkPrevBandsValue) && (price < sparkBandsValue)){
                result = true;
            }
            break;
        case "Cross Higher":
            if((price <= sparkPrevBandsValue) && (price > sparkBandsValue)){
                result = true;
            }
            break;
    }
    return result
}