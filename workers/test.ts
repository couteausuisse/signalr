import * as cheerio from "https://esm.sh/cheerio";
import puppeteer from "https://deno.land/x/puppeteer@16.2.0/mod.ts";
//import { serialize, deserialize } from "https://deno.land/x/serialize_javascript@v1.0.1/mod.ts";
import { serialize, deserialize } from "https://deno.land/x/serialize_simple@v1.1.0/mod.ts";

interface result {
    name: string, 
    value: number
}

const sleep = (ms: number) => new Promise((r) => setTimeout(r, ms));

console.log(await calculateMVRV())

async function calculateMVRV(){
    const url = 'https://stats.buybitcoinworldwide.com/realized-price/'//'https://charts.woobull.com/bitcoin-mvrv-ratio/' //'https://stats.buybitcoinworldwide.com/realized-price/'
    // try with this https://stats.buybitcoinworldwide.com/realized-price/
   /*  var trace3 = {
        type: "scatter",
        mode: "markers",
        name: 'Real / Actual',
        x: ['2011-01-01','2011-01-0
        Y */
    
    try {
        const browser = await puppeteer.launch({headless: false});
        const page = await browser.newPage();
        await page.goto(url);
        await sleep(10*1000);

        await page.screenshot({ path: "example.png" });
        // const res = await fetch(url);
        const html = await page.content();
        
        const $ = cheerio.load(html)  
        await browser.close();
        let test = $('body').html()//!.replace("Plotly.newPlot('chart', data, layout, {responsive: true});", '')
        console.log(test)


        const text = $('body').html(); // TODO there might be multiple script tags

        // find variable `x` in the text
        const matchX = text!.toString().match(/trace3 = (.*?)/);
        console.log(matchX!); // prints "{name: "Jeff"}"


        const pageText = $('body').html()


        console.log(pageText!.match(/trace3 = (\{.*?\{)/)!)
        const match = (String(pageText!.match(/mvrv = (\{.*?\{)/)![0]!)).replace('mvrv = ', '').replace(/\\n/g, '').replace(/\\/g, '').replace(",   marker: {","")
        const parsed = eval('(' + deserialize(match) + '})')
        const mvrv = parsed.y
        //console.log("Match", parsed)
        return [
            {
                name: Number(mvrv[ mvrv.length - 2 ]).toFixed(2), 
                value: Number(Number(mvrv[ mvrv.length - 2 ]).toFixed(2))
            }, {
                name: Number(mvrv[ mvrv.length - 1 ]).toFixed(2),
                value: Number(Number(mvrv[ mvrv.length - 1 ]).toFixed(2))
            }
        ] as result[]
        
    } catch(error) {
        console.log(error);
    }
}