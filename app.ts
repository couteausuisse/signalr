import { Application } from "https://deno.land/x/oak@v10.6.0/mod.ts";
import { viewEngine, engineFactory, adapterFactory } from "https://deno.land/x/view_engine@v10.6.0/mod.ts";
import { staticFileMiddleware } from "./middleware/staticServe.ts";
//import logger from "https://deno.land/x/oak_logger/mod.ts";
import { router } from "./middleware/routes.ts"
import { checkUpdate } from "./workers/update.ts"

interface AppState {
    accountId: number;
}
const handlebarsEngine = engineFactory.getHandlebarsEngine();
const oakAdapter = adapterFactory.getOakAdapter();
const app = new Application<AppState>();

await checkUpdate()

app.use(
    viewEngine(oakAdapter, handlebarsEngine, {
        viewRoot: "./view",
        viewExt: ".handlebars",
    })
);
  
app.use(router.routes());
app.use(router.allowedMethods());
app.use(staticFileMiddleware);

//app.use(logger.logger)
//app.use(logger.responseTime)

app.addEventListener("error", (evt) => {
    console.log(evt.error);
  });
  
console.log("App Ready: http://localhost:8000")
await app.listen({port:8000});
