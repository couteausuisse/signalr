FROM denoland/deno:1.40.2

# The port that your application listens to.
EXPOSE 8000

WORKDIR /app
# Prefer not to run as root.
#USER deno
# Cache the dependencies as a layer (the following two steps are re-run only when deps.ts is modified).
# Ideally cache deps.ts will download and compile _all_ external files used in main.ts.

RUN apt-get update 
#&& apt-get install cron -qq

COPY deps.ts .
RUN deno cache --unstable deps.ts

# These steps will be re-run upon each file change in your working directory:
COPY . .
#COPY ./crontab.example /etc/cron.d/container_cronjob

# Give execution rights on the cron job
#RUN chmod 0644 /etc/cron.d/container_cronjob

# Apply cron job
#RUN crontab /etc/cron.d/container_cronjob
 
# Create the log file to be able to run tail
#RUN touch /var/log/cron.log

# Compile the main app so that it doesn't need to be compiled each startup/entry.
RUN deno cache --unstable app.ts
RUN deno cache --unstable workers/update.ts
RUN deno cache --unstable workers/signals.ts
RUN deno cache --unstable workers/sparks.ts

#CMD cron && service cron start && ./start.sh
CMD ./start.sh